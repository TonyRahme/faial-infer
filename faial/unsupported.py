__all__ = "rewrite_unsupported"

from .traverse import *
from .c_ast import *

UNSUPPORTED_TYPES = {
    "CompoundAssignOperator",
    "UnaryOperator",
}

UNARY_CONVERT_OPCODE = {
    "++": "+",
    "--": "-",
    "-": "-",
}

def convert_compound_assign(obj):
    type = obj.get("computeResultType",None)
    lhs = (obj.get("lhs",None))
    rhs = obj.get("rhs",None)
    opcode = obj.get("opcode",None)[:-1]

    obj["kind"] = "BinaryOperator"
    obj["lhs"] = lhs
    obj["rhs"] = binary_operator(opcode,lhs,rhs,type)
    obj["opcode"] = "="
    # if "computeLHSType" in obj:
    #     del obj["computeLHSType"]
    # if "computeResultType" in obj:
    #     del obj["computeResultType"]

def convert_unary(obj):
    opcode = obj.get("opcode",None)
    postfix = obj.get("isPostfix",None)
    if opcode in UNARY_CONVERT_OPCODE:
        if(postfix):
            type = obj.get("type",None)
            lhs = obj.get("subExpr", None)
            new_opcode = UNARY_CONVERT_OPCODE[opcode]
            obj["lhs"] = lhs
            obj["rhs"] = binary_operator(new_opcode,lhs,integer_literal(1),type)
            obj["opcode"] = "="

        else:
            rhs = obj.get("subExpr",None)
            obj["lhs"] = integer_literal(0)
            obj["rhs"] = rhs

        obj["kind"] = "BinaryOperator"
        del obj["isPostfix"]
        del obj["subExpr"]


CONVERT_TYPES = {
    "CompoundAssignOperator": convert_compound_assign,
    "UnaryOperator": convert_unary,
}

def rewrite_unsupported(data):
    for obj in walk(data):
        obj_kind = obj.get("kind", None)
        handle_types = CONVERT_TYPES.get(obj_kind, None)
        if handle_types is not None:
            handle_types(obj)
