UNARY_ASSIGN = {
    '++',
    '--',
}

BINARY_ASSIGN = {
    '^=',
    '+=',
    '-=',
    '*=',
    '/=',
}

def is_kind(obj, kind):
  return isinstance(obj, dict) and obj.get("kind", None) == kind

def match(obj, **kwargs):
    return all(obj.get(k, None) == v for k,v in kwargs.items())

def is_var(obj):
    return match(obj, kind="ParmVarDecl") or match(obj, kind="VarDecl")

def is_integer_literal(obj):
    return is_kind(obj, "IntegerLiteral")

def is_while_stmt(obj):
    return is_kind(obj, "WhileStmt")

def is_if_stmt(obj):
    return is_kind(obj, "IfStmt")

def is_comma_operator(obj):
    return is_kind(obj, "BinaryOperator") and obj.get('opcode', None) == ','

def is_assign_unary(obj):
    return is_kind(obj, "UnaryOperator") and obj.get('opcode', None) in UNARY_ASSIGN

def is_operator_equal(obj):
    return obj is not None and is_kind(obj, "CXXMethodDecl") and obj.get('name', None) == 'operator='

def is_assign_binary(obj):
    return \
        (is_kind(obj, "BinaryOperator") and obj.get('opcode', None) == '=') or \
        (is_kind(obj, "CompoundAssignOperator") and obj.get('opcode', None) in BINARY_ASSIGN)


def is_assign_binary(obj):
    return \
        (is_kind(obj, "BinaryOperator") and obj.get('opcode', None) == '=') or \
        (is_kind(obj, "CompoundAssignOperator") and obj.get('opcode', None) in BINARY_ASSIGN)

def is_decl_stmt(obj):
    return is_kind(obj, "DeclStmt")

def is_for_stmt(obj):
    return is_kind(obj, "ForStmt")

def is_function_decl(obj):
    return is_kind(obj, "FunctionDecl")

def is_method_call_expr(obj):
     return is_kind(obj, "CXXOperatorCallExpr")

def is_call_expr(obj):
    return is_kind(obj, "CallExpr")

def is_func_call_expr(obj, name):
    if not is_call_expr(obj):
        return False
    func = obj.get("func", None)
    if func is None:
        return False
    return is_function_decl(func) and func.get("name", "") == name

def is_annotate_attr(obj):
    return is_kind(obj, "AnnotateAttr")

def is_annotate_attr_ex(elem, key):
    tmp = '__attribute__((annotate("%s")))' % key
    return is_annotate_attr(elem) and elem.get('value', "").strip() == tmp

def is_compound_stmt(obj):
    return is_kind(obj, "CompoundStmt")

def compound_stmt(body=None):
    if body is None:
        body = []
    return {
        "kind": "CompoundStmt",
        "inner": body
    }

def call_expr(name, args=()):
    return {
        "kind": "CallExpr",
        "func": name,
        "args": args,
    }

def integer_literal(num):
    return {
        "kind": "IntegerLiteral",
        "type": {"qualType": "int"},
        "value": num,
    }

def int_type():
    return {"qualType": "int"}

def bool_type():
    return dict(qualType="bool")

def binary_operator(opcode, lhs, rhs, type=None):
    result = dict(
        kind="BinaryOperator",
        opcode=opcode,
        lhs=lhs,
        rhs=rhs,
    )
    if type is not None:
        result["type"] = type
    return result

def compound_assign_operator(opcode, lhs, rhs, type=None):
    result = dict(
        kind="CompoundAssignOperator",
        opcode=opcode,
        lhs=lhs,
        rhs=rhs,
    )
    if type is not None:
        result["type"] = type
    return result

def comma_operator(lhs, rhs, type=None):
    return binary_operator(opcode=",", lhs=lhs, rhs=rhs, type=type)

def plus_operator(lhs, rhs, type=None):
    return binary_operator(opcode="+", lhs=lhs, rhs=rhs, type=type)

def minus_operator(lhs, rhs, type=None):
    return binary_operator(opcode="-", lhs=lhs, rhs=rhs, type=type)

def multiply_operator(lhs, rhs, type=None):
    return binary_operator(opcode="*", lhs=lhs, rhs=rhs, type=type)

def divide_operator(lhs, rhs, type=None):
    return binary_operator(opcode="/", lhs=lhs, rhs=rhs, type=type)

def lt_operator(lhs, rhs):
    return binary_operator(opcode="<", lhs=lhs, rhs=rhs, type="bool")

def and_operator(lhs, rhs):
    return binary_operator(opcode="&&", lhs=lhs, rhs=rhs, type="bool")

def le_operator(lhs, rhs):
    return binary_operator(opcode="<=", lhs=lhs, rhs=rhs, type="bool")

def gt_operator(lhs, rhs):
    return binary_operator(opcode=">", lhs=lhs, rhs=rhs, type="bool")

def ge_operator(lhs, rhs):
    return binary_operator(opcode=">=", lhs=lhs, rhs=rhs, type="bool")

def left_shift_operator(lhs, rhs):
    return binary_operator(opcode="<<", lhs=lhs, rhs=rhs, type=type)

def right_shift_operator(lhs, rhs):
    return binary_operator(opcode=">>", lhs=lhs, rhs=rhs, type=type)

def inc_operator(expr, type=None):
    return plus_operator(
        lhs=expr,
        rhs=integer_literal(1),
        type=type,
    )

def assert_stmt(cnd):
    return dict(cond=cnd, kind="AssertStmt")

def distinct_expr(elems):
    return dict(args=elems, kind="DistinctExpr")

def decl_stmt(var):
    return dict(kind="DeclStmt", inner=[var])

def decl_stmt_var(name, type=None, init=None):
    return decl_stmt(var_decl(name, type=type, init=init))

def for_stmt(init,cond,inc,body,condVar=None):
    if condVar is None:
        condVar = {}
    return dict(
        kind="ForStmt",
        init=init,
        cond=cond,
        inc=inc,
        condVar=condVar,
        body=body)

def var_decl(name, type=None, init=None):
    result = dict(
        kind="VarDecl",
        name=name,
    )
    if type is None and "type" in init:
        type = dict(init["type"])

    if type is not None:
        result["type"] = type

    if init is not None:
        result["inner"] = [init]
        result["init"] = "c"

    return result

def array_subscript(lhs, rhs, type=None):
    return dict(kind="ArraySubscriptExpr", lhs=lhs, rhs=rhs, type=type)

def unary_operator(opcode, expr, type=None, is_postfix=True):
    result = dict(
        kind="UnaryOperator",
        opcode=opcode,
        subExpr=expr,
        isPostfix=is_postfix,
    )
    if type is not None:
        result["type"] = type
    return result

def not_operator(expr):
    result = unary_operator("!", expr, type=bool_type(), is_postfix=True)
    result["canOverflow"] = False
    return result

def plus_plus_operator(expr, type=None, is_postfix=True):
    if type is None:
        type = int_type()
    return unary_operator("++", expr, type=type, is_postfix=is_postfix)

def minus_minus_operator(expr, type=None, is_postfix=True):
    if type is None:
        type = int_type()
    return unary_operator("--", expr, type=type, is_postfix=is_postfix)

def plus_plus_var(name, type=None, is_postfix=True):
    if type is None:
        type = int_type()
    return plus_plus_operator(var_decl(name, type=type),
        type=type,
        is_postfix=is_postfix)

def minus_minus_var(name, type=None, is_postfix=True):
    if type is None:
        type = int_type()
    return minus_minus_operator(var_decl(name, type=type),
        type=type,
        is_postfix=is_postfix)

def equal_equal_operator(lhs, rhs, type=None):
    return compound_assign_operator(opcode='==',lhs=lhs,rhs=rhs,type=type)

def plus_equal_operator(lhs, rhs, type=None):
    return compound_assign_operator(opcode='+=',lhs=lhs,rhs=rhs,type=type)

def minus_equal_operator(lhs, rhs, type=None):
    return compound_assign_operator(opcode='-=',lhs=lhs,rhs=rhs,type=type)

def multiply_equal_operator(lhs, rhs, type=None):
    return compound_assign_operator(opcode="*=", lhs=lhs, rhs=rhs, type=type)

def divide_equal_operator(lhs, rhs, type=None):
    return compound_assign_operator(opcode="/=", lhs=lhs, rhs=rhs, type=type)

def right_shift_equal_operator(lhs, rhs, type=None):
    return compound_assign_operator(opcode=">>=", lhs=lhs , rhs=rhs, type=type)

def left_shift_equal_operator(lhs, rhs, type=None):
    return compound_assign_operator(opcode="<<=", lhs=lhs , rhs=rhs, type=type)
