from .traverse import *
from .c_ast import *

__all__ = ["rewrite_access"]

"""
Memory access inference.
"""

# kind: BinaryOperator
#       lhs:
#         kind: ParmVarDecl
#         name: dst
#         type:
#           qualType: float *
#       opcode: '='

def is_pointer(obj):
    if not isinstance(obj, dict):
        return False
    ty = obj.get("type", None)
    if ty is None or not isinstance(ty, dict):
        return False
    return ty.get("qualType", "").endswith(" *")

def is_decl_ptr(obj):
    return (
        is_kind(obj, 'DeclStmt') and
        "inner" in obj and
        len(obj["inner"]) > 0 and
        is_pointer(obj["inner"][0])
    )

def is_assign_ptr(obj):
    return (
        is_kind(obj, 'BinaryOperator')
        and
        obj["opcode"] == "="
        #and
        #is_pointer(obj)
    )

def is_nd_access(obj):
    if is_kind(obj, 'ArraySubscriptExpr'):
        if is_var(obj['lhs']):
            return True
        return is_nd_access(obj['lhs'])
    return False

def get_accesses(obj):
    return find(obj, is_nd_access)

def access(loc, index, mode):
    return {
        "kind": "AccessStmt",
        "location": loc,
        "mode": mode,
        "index": index,
    }

def load_offset(loc):
    return {
        "kind": "LocationAliasStmt",
        "target": None,
        "source": loc,
        "offset": integer_literal(0),
    }

RW_MODE = 'rw'
RO_MODE = 'ro'

OPS = {
    "+": plus_operator,
    "-": minus_operator,
    "*": multiply_operator,
    "/": divide_operator,
}

def parse_load_expr(obj):
    if is_var(obj) and is_pointer(obj):
        return load_offset(obj)
    if is_kind(obj, 'BinaryOperator'):
        lhs = parse_load_expr(obj["lhs"])
        rhs = parse_load_expr(obj["rhs"])
        if lhs["kind"] == "LocationAliasStmt":
            lhs["offset"] = OPS[obj["opcode"]](lhs["offset"], rhs)
            return lhs
        else:
            obj["lhs"] = lhs
            obj["rhs"] = rhs
    return obj


def to_access(obj, mode):
    def aux(obj, indices):
        assert obj['kind'] == 'ArraySubscriptExpr'
        child = obj['lhs']
        indices.append(obj['rhs'])
        if is_var(child):
            return access(child["name"], indices, mode)
        else:
            return aux(child, indices)
    return aux(obj, [])

def to_rw_access(obj):
    return to_access(obj, RW_MODE)

def to_ro_access(obj):
    return to_access(obj, RO_MODE)

def rewrite_access(obj):
    def do_become(obj, elems):
        if len(elems) == 0:
            return
        if len(elems) == 1:
            stmt = elems[0]
        else:
            stmt = compound_stmt(elems)
        become(obj, stmt)

    for eq_obj in filter(is_assign_binary, walk(obj)):
        if is_pointer(eq_obj):
            if eq_obj["kind"] == "CompoundAssignOperator":
                target = eq_obj["lhs"]
                offset = eq_obj["rhs"]
                alias = parse_load_expr(target)
                alias["target"] = target
                alias["offset"] = offset
                do_become(eq_obj, alias)
            elif eq_obj["kind"] == "BinaryOperator":
                target = eq_obj["lhs"]
                alias = parse_load_expr(eq_obj["rhs"])
                alias["target"] = target
                do_become(eq_obj, alias)
        else:
            # READS
            accs = list(map(to_rw_access, get_accesses(eq_obj["lhs"])))
            # WRITES
            accs.extend(map(to_ro_access, get_accesses(eq_obj["rhs"])))
            do_become(eq_obj, accs)

    for loop in filter(lambda x: is_while_stmt(x) or is_if_stmt(x), walk(obj)):
        accs = list(map(to_rw_access, get_accesses(loop["cond"])))
        if len(accs) > 0:
            loop["cond"] = equal_equal_operator(integer_literal(1), integer_literal(1))
            other = dict()
            other.update(loop)
            accs.append(other)
            for acc in accs:
                acc["mode"] = "ro"
            do_become(loop, accs)

    for obj_update in filter((lambda x: is_method_call_expr(x) or is_call_expr(obj)), walk(obj)):
        accs = list(map(to_ro_access, get_accesses(obj_update["args"])))
        do_become(obj_update, accs)

    for child in filter(is_decl_stmt, walk(obj)):
        if is_decl_ptr(child):
            inner = child["inner"][0]["inner"]
            asgn = parse_load_expr(inner[0])
            del child["inner"][0]["inner"]
            asgn["target"] = child["inner"][0]
            do_become(child, asgn)
        else:
            accs = list(map(to_ro_access, get_accesses(child["inner"][0])))
            do_become(child, accs)
    for child in filter(is_nd_access, walk(obj)):
        do_become(child, to_ro_access(child))
