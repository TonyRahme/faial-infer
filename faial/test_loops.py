from .c_ast import *
from .loops import infer_range, range_expr

def test_infer_ok():
    # for (i = 0; i < 100; i++)
    loop = for_stmt(
        init=decl_stmt_var("i", init=integer_literal(0)),
        cond=lt_operator(
            var_decl("i", type=int_type()),
            integer_literal(100)
        ),
        inc=plus_plus_var("i"),
        body=compound_stmt(),
    )
    var, rng = infer_range(loop)
    assert var_decl("i", type=int_type()) == var
    assert rng == range_expr(
     init = integer_literal(0),
     upper_bound = integer_literal(100),
     opcode = '+',
     step = integer_literal(1),
    )

def test_oks():
    """
    Testing range inference; we really do not care about the variable
    being inferred here.
    """
    # Each element of the list is a triple: a string documenting the
    # loop, the given for statement, the expected range to be inferred.
    ok_loops = [
        [
            """
            for (i = 0; i < 100; i++)
            """,
            # Given
            for_stmt(
                init=decl_stmt_var("i", init=integer_literal(0)),
                cond=lt_operator(
                    var_decl("i", type=int_type()),
                    integer_literal(100)
                ),
                inc=plus_plus_var("i"),
                body=compound_stmt(),
            ),
            # Expected
            range_expr(
                init = integer_literal(0),
                upper_bound = integer_literal(100),
                opcode = '+',
                step = integer_literal(1),
            )
        ],
        [
            """
            for (i = 0; i <= 100; i++)
            """,
            # Given
            for_stmt(
                init=decl_stmt_var("i", init=integer_literal(0)),
                cond=le_operator(
                    var_decl("i", type=int_type()),
                    integer_literal(100)
                ),
                inc=plus_plus_var("i"),
                body=compound_stmt(),
            ),
            # Expected
            range_expr(
                init = integer_literal(0),
                # Upper-bound + 1
                upper_bound = inc_operator(integer_literal(100)),
                opcode = '+',
                step = integer_literal(1),
            )
        ],
        [
            """
            for (i = -4; i <= 100; i++)
            """,
            # Given
            for_stmt(
                init=decl_stmt_var("i", init=integer_literal(-4)),
                cond=le_operator(
                    var_decl("i", type=int_type()),
                    integer_literal(100)
                ),
                inc=plus_plus_var("i"),
                body=compound_stmt(),
            ),
            # Expected
            range_expr(
                init = integer_literal(-4),
                # Upper-bound + 1
                upper_bound = inc_operator(integer_literal(100)),
                opcode = '+',
                step = integer_literal(1),
            )
        ],
        [
            """
            for (i = 100; i > 0; i--)
            """,
            # Given
            for_stmt(
                init=decl_stmt_var("i", init=integer_literal(100)),
                cond=gt_operator(
                    var_decl("i", type=int_type()),
                    integer_literal(0)
                ),
                inc=minus_minus_var("i"),
                body=compound_stmt(),
            ),
            # Expected
            range_expr(
                init = inc_operator(integer_literal(0)),
                # Upper-bound + 1
                upper_bound = inc_operator(integer_literal(100)),
                opcode = '+',
                step = integer_literal(1),
            )
        ],
        [
            """
            for (i = 100; i >= 0; i--)
            """,
            # Given
            for_stmt(
                init=decl_stmt_var("i", init=integer_literal(100)),
                cond=ge_operator(
                    var_decl("i", type=int_type()),
                    integer_literal(0)
                ),
                inc=minus_minus_var("i"),
                body=compound_stmt(),
            ),
            # Expected
            range_expr(
                init = integer_literal(0),
                # Upper-bound + 1
                upper_bound = inc_operator(integer_literal(100)),
                opcode = '+',
                step = integer_literal(1),
            )
        ],
        [
            """
            for (i = 0; i < 100; i+=2)
            """,
            # Given
            for_stmt(
                init=decl_stmt_var("i", init=integer_literal(0)),
                cond=lt_operator(
                    var_decl("i", type=int_type()),
                    integer_literal(100)
                ),
                inc=plus_equal_operator("i",integer_literal(2)),
                body=compound_stmt(),
            ),
            # Expected
            range_expr(
                init = integer_literal(0),
                upper_bound = integer_literal(100),
                opcode = '+',
                step = integer_literal(2),
            )
        ],
        [
            """
            for (i = 100; i > 0; i-=3)
            """,
            # Given
            for_stmt(
                init=decl_stmt_var("i", init=integer_literal(100)),
                cond=gt_operator(
                    var_decl("i", type=int_type()),
                    integer_literal(0)
                ),
                inc=minus_equal_operator("i",integer_literal(3)),
                body=compound_stmt(),
            ),
            # Expected
            range_expr(
                init = inc_operator(integer_literal(0)),
                upper_bound = inc_operator(integer_literal(100)),
                opcode = '+',
                step = integer_literal(3),
            )
        ],
        [
            """
            for (i = 0; i < 100; i*=3)
            """,
            # Given
            for_stmt(
                init=decl_stmt_var("i", init=integer_literal(0)),
                cond=lt_operator(
                    var_decl("i", type=int_type()),
                    integer_literal(100)
                ),
                inc=multiply_equal_operator("i",integer_literal(3)),
                body=compound_stmt(),
            ),
            # Expected
            range_expr(
                init = integer_literal(0),
                upper_bound = integer_literal(100),
                opcode = 'pow3',
                step = integer_literal(1),
            )
        ],
        [
            """
            for (i = 100; i > 0; i/=3)
            """,
            # Given
            for_stmt(
                init=decl_stmt_var("i", init=integer_literal(100)),
                cond=gt_operator(
                    var_decl("i", type=int_type()),
                    integer_literal(0)
                ),
                inc=divide_equal_operator("i",integer_literal(3)),
                body=compound_stmt(),
            ),
            # Expected
            range_expr(
                init = inc_operator(integer_literal(0)),
                upper_bound = inc_operator(integer_literal(100)),
                opcode = 'pow3',
                step = integer_literal(1),
            )
        ],
        [
            """
            for (i = 100; i > 0; i>>=2)
            """,
            # Given
            for_stmt(
                init=decl_stmt_var("i", init=integer_literal(100)),
                cond=gt_operator(
                    var_decl("i", type=int_type()),
                    integer_literal(0)
                ),
                inc=right_shift_equal_operator("i",integer_literal(2)),
                body=compound_stmt(),
            ),
            # Expected
            range_expr(
                init = inc_operator(integer_literal(0)),
                upper_bound = inc_operator(integer_literal(100)),
                opcode = 'pow2',
                step = integer_literal(2),
            )
        ],
        [
            """
            for (i = 0; i <= 100; i<<=3)
            """,
            # Given
            for_stmt(
                init=decl_stmt_var("i", init=integer_literal(0)),
                cond=le_operator(
                    var_decl("i", type=int_type()),
                    integer_literal(100)
                ),
                inc=left_shift_equal_operator("i",integer_literal(3)),
                body=compound_stmt(),
            ),
            # Expected
            range_expr(
                init = integer_literal(0),
                upper_bound = inc_operator(integer_literal(100)),
                opcode = 'pow2',
                step = integer_literal(3),
            )
        ],
        [
            """
            for (i = 0; i <= 100)
            """,
            # Given
            for_stmt(
                init=decl_stmt_var("i", init=integer_literal(0)),
                cond=le_operator(
                    var_decl("i", type=int_type()),
                    integer_literal(100)
                ),
                inc=None,
                body=compound_stmt(),
            ),
            # Expected
            range_expr(
                init = integer_literal(0),
                upper_bound = inc_operator(integer_literal(100)),
                opcode = '+',
                step = integer_literal(1),
            )
        ],
        [
            """
            for (;i > 0;i--)
            """,
            # Given
            for_stmt(
                init=None,
                cond=gt_operator(
                    var_decl("i", type=int_type()),
                    integer_literal(0)
                ),
                inc=None,
                body=compound_stmt(),
            ),
            # Expected
            range_expr(
                init = inc_operator(integer_literal(0)),
                # Upper-bound + 1
                upper_bound = inc_operator(integer_literal(0xfffffffe)),
                opcode = '+',
                step = integer_literal(1),
            )
        ],
        [
            """
            for (i = 0; i < 100;i++,b++)
            """,
            # Given
            for_stmt(
                init=decl_stmt_var("i", init=integer_literal(0)),
                cond=lt_operator(
                    var_decl("i", type=int_type()),
                    integer_literal(100)
                ),
                inc=comma_operator(plus_plus_var("b"), plus_plus_var("i")),
                body=compound_stmt(),
            ),
            # Expected
            range_expr(
                init = integer_literal(0),
                upper_bound = integer_literal(100),
                opcode = '+',
                step = integer_literal(1),
            )
        ],
    ]
    for (msg, given, expected) in ok_loops:
        msg = msg.strip()
        _, rng = infer_range(given)
        assert rng == expected, msg
