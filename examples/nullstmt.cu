//pass
//--gridDim=1 --blockDim=512

#define             INV_SQRT_2      0.70710678118654752440f;


__global__ void
dwtHaar1D(float *id, float *od, float *approx_final,
          const unsigned int dlevels,
          const unsigned int slength_step_half,
          const int bdim)
{
    // shared memory for part of the signal
    extern __shared__ float shared[];

    const int tid = threadIdx.x + blockIdx.x * blockDim.x;

    float data0 = shared[2*tid];
    float data1 = shared[(2*tid) + 1];
    __syncthreads();

    shared[tid] = (data0 + data1) * INV_SQRT_2;
}
