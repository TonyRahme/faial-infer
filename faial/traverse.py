"""
Utility function to traverse tree of objects (dicts/maps).
"""

def has_children(obj):
    return isinstance(obj, dict) or isinstance(obj, list)

def get_children(obj):
    if isinstance(obj, dict) and "kind" in obj:
        return tuple(obj.values())
    elif isinstance(obj, list):
        return tuple(obj)
    return ()

def find(obj, cond):
    """
    XXX:
    """
    todo = [obj]
    visited = []
    while len(todo) > 0:
        obj = todo.pop()
        if isinstance(obj, dict) and cond(obj):
            visited.append(obj)
        else:
            for child in get_children(obj):
                if has_children(child):
                    todo.append(child)
    visited.reverse()
    return visited


def walk(obj):
    """
    Iterate over all dict/list objects in DFS order.
    """
    todo = [obj]
    visited = []
    while len(todo) > 0:
        obj = todo.pop()
        if isinstance(obj, dict):
            visited.append(obj)
        for child in get_children(obj):
            if has_children(child):
                todo.append(child)
    visited.reverse()
    return visited

def walk2(obj):
    """
    Iterate over all dict/list objects in DFS order.
    """
    todo = [(obj,None)]
    visited = []
    while len(todo) > 0:
        obj, parent = todo.pop()
        if isinstance(obj, dict):
            visited.append((obj,parent))
        for child in get_children(obj):
            if has_children(child):
                todo.append((child, obj))
    visited.reverse()
    return visited

def remove_all_matching(pred,obj):
    todo = [obj]
    visited = []
    while len(todo) > 0:
        obj = todo.pop()
        if len(get_children(obj)) > 0:
            visited.append(obj)
        for child in get_children(obj):
            if has_children(child):
                todo.append(child)
    visited.reverse()

    removed = 0
    for child in visited:
        if isinstance(child, dict) or isinstance(child, list):
            removed += len(remove_child_if(pred, child))
    return removed


def become(obj1, obj2):
    obj1.clear()
    obj1.update(obj2)

def dict_remove_child_if(pred, elems):
    to_remove = []
    for k,v in elems.items():
        if pred(v):
            to_remove.append(k)
    for k in to_remove:
        del elems[k]
    return to_remove

def list_remove_child_if(pred, elems):
    offset = 0
    removed = list(filter(lambda x: pred(x[1]), enumerate(elems)))
    for idx, elem in removed:
        del elems[idx - offset]
        offset += 1
    return removed

def remove_child_if(pred, elems):
    """
    Removes all elements matching a given predicate.
    Returns a list that consists of pairs with the index of the original list
    and the removed element.
    """
    if isinstance(elems, dict):
        return dict_remove_child_if(pred, elems)
    elif isinstance(elems, list):
        return list_remove_child_if(pred, elems)
    raise ValueError(elems)

def remove_all_empty_dicts(data):
    remove_all_matching(lambda x: isinstance(x, dict) and len(x) == 0, data)

def find_bug(obj):
    for child, parent in walk2(obj):
        if isinstance(child, dict) and len(child) == 0:
            remove_all_empty_dicts(parent)
            import sys
            for k,v in enumerate(parent):
                if v is child:
                    print(k)
            print("BUG")
            sys.exit(1)
