POETRY = poetry

all:

sdist:
	$(POETRY) build

deps:
	$(POETRY) install

test:
	$(POETRY) run pytest

.PHONY: all deps test