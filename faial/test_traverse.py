from .traverse import *

def test_remove_all_empty_dicts():
    b = {"kind": "foo", "one":["twelve",{}],"two":2}
    remove_all_empty_dicts(b)
    actual = b["one"]
    expected = ["twelve"]
    assert actual == expected

def test_get_children():
    a = {"kind":{"two":2}}

    actual = get_children(a)
    expected = ({"two":2},)
    assert actual == expected
