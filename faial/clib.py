__all__ = "clean_c", "FILTER_KEY_LOCATION", "is_kind"

from .traverse import *
from .c_ast import is_kind, not_operator
from .filter_record import *

COLLAPSE = {
    "SubstNonTypeTemplateParmExpr",
    "DeclRefExpr",
    "CXXConstructExpr",
    "ParenExpr",
    "ImplicitCastExpr",
    "CXXStaticCastExpr",
    "CStyleCastExpr",
    "ExprWithCleanups",
    "LabelStmt",
}

FLATTEN_OBJS = {
    'BinaryOperator': ("lhs", "rhs"),
    'MemberExpr': ("base",),
    'IfStmt': ("cond", "thenStmt", "elseStmt"),
    'ArraySubscriptExpr': ("lhs", "rhs"),
    'CompoundAssignOperator': ("lhs", "rhs"),
    'ForStmt': ("init", "condVar", "cond", "inc", "body"),
    'WhileStmt': ("cond", "body"),
    'DoStmt': ("body", "cond"),
    'ReturnStmt': ("retExpr",),
    'UnaryOperator': ("subExpr",),
    'ConditionalOperator': ("cond", "thenExpr", "elseExpr"),
}

def flatten_function_decl(obj, inner):
    params = []
    attrs = []
    body = []
    elems = []
    unknown = []
    for elem in inner:
        kind = elem.get("kind", "")
        if len(elem) == 0:
            # We safely filter out empty dictionary as they represent
            # elements to be removed.
            # Some flattening step may yield an empty ASTs which is the only
            # way we have of flagging an element for removal.
            continue
        elif kind == "ParmVarDecl" or kind == "TemplateArgument":
            params.append(elem)
        elif kind.endswith("Attr"):
            attrs.append(elem)
        elif kind.endswith("Stmt") or kind.endswith("Operator") or kind.endswith("Expr"):
            body.append(elem)
        else:
            unknown.append(elem)
    if len(params) == 0 and len(attrs) == 0 and len(body) == 0:
        return
    if len(body) > 1:
        raise ValueError("Function body has more than one elements", obj, body)

    obj["params"] = params
    obj["attrs"] = attrs
    obj["body"] = body[0] if len(body) == 1 else None
    if len(unknown) > 0:
        raise ValueError("Unknown elements of function", unknown)

    return True

def flatten_function_template_decl(obj, inner):
    args = []
    for elem in inner:
        if elem.get("kind", None) == "NonTypeTemplateParmDecl":
            args.append(elem)
    for elem in inner:
        if elem.get("kind", None) == "FunctionDecl":
            obj.clear()
            for k in elem:
                obj[k] = elem[k]
            obj["params"].extend(args)
            return

def has_void_type(obj):
    ty = obj.get("type", None)
    return ty is not None and ty.get("qualType", None) == "void"


def post_compound_stmt(obj):
    if "inner" not in obj:
        obj["inner"] = []

def post_flatten_conditional_operator(obj):
    if has_void_type(obj) and has_void_type(obj["elseExpr"]):
        else_expr = obj["elseExpr"]
        if else_expr.get("kind", None) == "CallExpr" and \
                    else_expr["func"]["name"] == "__assert_fail":
            # We have an assert
            obj["kind"] = "AssertStmt"
            del obj["elseExpr"]
            del obj["thenExpr"]

def post_infer_assert(obj):
    then_expr = obj.get("thenStmt", None)
    if obj.get("elseStmt", None) is None and then_expr is not None and then_expr.get("kind", None) == "ReturnStmt":
        del obj["thenStmt"]
        obj["cond"] = not_operator(obj["cond"])
        obj["kind"] = "AssertStmt"


def flatten_call_expr(obj, inner):
    func, args = inner[0], inner[1:]
    obj["func"] = func
    obj["args"] = args
    if func.get("name", "") == '__requires' and len(args) == 1:
        obj["cond"] = args[0]
        obj["kind"] = "AssertStmt"
        del obj["func"]
        del obj["args"]
    elif func.get("name", "") == '__is_pow2' and len(args) == 1:
        obj["subExpr"] = args[0]
        obj["opcode"] = "pow2"
        obj["kind"] = "PredicateExpr"
        del obj["func"]
        del obj["args"]

def post_flatten_binop(obj):
    if obj.get("opcode") == "<<" and is_kind(obj["rhs"], 'IntegerLiteral'):
        obj["opcode"] = "*"
        obj["rhs"]["value"] = 2 ** obj["rhs"]["value"]
    elif obj.get("opcode") == ">>" and is_kind(obj["rhs"], 'IntegerLiteral'):
        obj["opcode"] = "/"
        obj["rhs"]["value"] = 2 ** obj["rhs"]["value"]

def post_flatten_attributed_stmt(obj):
    obj["kind"] = "CompoundStmt"
    obj["inner"] = [obj["inner"][1]]

FLATTEN_OBJS_EX = {
    "FunctionTemplateDecl": flatten_function_template_decl,
    "FunctionDecl": flatten_function_decl,
    "CallExpr": flatten_call_expr,
    "CXXOperatorCallExpr": flatten_call_expr,
}

POST_FLATTEN_OBJS = {
    "CompoundStmt": post_compound_stmt,
    "AttributedStmt": post_flatten_attributed_stmt,
    "BinaryOperator": post_flatten_binop,
    "ConditionalOperator": post_flatten_conditional_operator,
    "IfStmt": post_infer_assert,
}

SKIP_RECORD = {
    "__skip_start",
    "__skip_end",
}

FILTER_KIND = {
    "FullComment",
    "TextComment",
    "LinkageSpecDecl",
    "NullStmt",
    "CXXRecordDecl",
    "EnumConstantDecl",
    "EnumDecl",
    "ClassTemplateSpecializationDecl",
}

FILTER_KEY = {
    "id",
    "referencedMemberDecl",
    "valueCategory",
}

FILTER_KEY_LOCATION = {"range", "loc"}

def remove_key(obj, key):
    if key in obj:
        del obj[key]

def remove_keys(obj, keys):
    for key in keys:
        remove_key(obj, key)

def collapse_obj(obj):
    child, = obj["inner"]
    obj.clear()
    obj.update(child)

def collapsable(obj):
    if "kind" not in obj or "inner" not in obj:
        return False
    return \
      (obj["kind"] == "CompoundStmt" and len(obj["inner"]) == 1) \
      or obj["kind"] in COLLAPSE

def do_variables(obj):
    if is_kind(obj, 'DeclRefExpr'):
        child = obj["referencedDecl"]
        obj.clear()
        obj.update(child)

def do_collapse(obj):
    if isinstance(obj, dict) and collapsable(obj):
        collapse_obj(obj)
    do_variables(obj)

def interpolate(obj):
    if is_kind(obj, 'IntegerLiteral'):
        obj["value"] = int(obj["value"])

def do_flatten(obj):
    if not isinstance(obj, dict) or "kind" not in obj:
        return
    obj_kind = obj.get("kind", None)
    handle_ex = FLATTEN_OBJS_EX.get(obj_kind, None)
    if handle_ex is not None:
        children = obj.get("inner", [])
        handle_ex(obj, children)
        if "inner" in obj:
            del obj["inner"]
    else:
        fields = FLATTEN_OBJS.get(obj_kind, ())
        if len(fields) == 0 or "inner" not in obj:
            return
        children = obj["inner"]
        del obj["inner"]
        for (k, v) in zip(fields, children):
            obj[k] = v
        children.clear()


def filter_kind(obj):
    if obj.get("kind", None) in FILTER_KIND:
        obj.clear()

def merge(map1, map2):
    result = dict(map1)
    for k,v in map2.items():
        result[k] = map1.get(k, set()).union(v)
    return result

def filter_cxx_record_decl(obj):
    return is_kind(obj, "CXXRecordDecl") \
        and obj.get("tagUsed", None) == "struct" \
        and "name" not in obj

def filter_records(obj, record_filter):
    to_remove = []
    children = obj["inner"]
    for idx, rec in enumerate(children):
        rec_kind = rec["kind"]
        names = record_filter.get(rec_kind, ())
        if rec.get("name", None) in names or filter_cxx_record_decl(rec):
            to_remove.append(idx)

    to_remove.reverse()
    for idx in to_remove:
        del children[idx]

# Remove elements in between __skip_start and __skip_end
def remove_skips(obj):
    if (obj.get("kind",None)=="CompoundStmt"):
        removal_index = []
        if("inner" in obj):
            children = obj["inner"]
            skipping = False
            for idx, val in enumerate(children):
                val_kind = val.get("kind",None)
                if(val_kind=="CallExpr"):
                    val_funcName = val.get("func",None).get("name",None)
                    if(val_funcName=="__skip_start"):
                        skipping = True
                    if(val_funcName=="__skip_end"):
                        removal_index.append(idx)
                        skipping = False
                if(skipping):
                    removal_index.append(idx)

            removal_index.reverse()
            for idx in removal_index:
                del children[idx]

def clean_c(data, key_filter=None, record_filter=None):
    if record_filter is None:
        record_filter = {}
    if key_filter is None:
        key_filter = set()

    filter_records(data, merge(FILTER_RECORD, record_filter))

    for obj in walk(data):
        remove_keys(obj, FILTER_KEY.union(key_filter))
        # Filter certain kinds of objects
        filter_kind(obj)
        # Convert integers to ints
        interpolate(obj)
        # Convert inner to fields
        do_flatten(obj)
        # Collapse container objects that only have one child in their inner
        do_collapse(obj)
        # Remove all code in between a skip_start and skip_end
        remove_skips(obj)

    for obj in walk(data):
        obj_kind = obj.get("kind", None)
        if obj_kind in POST_FLATTEN_OBJS:
            POST_FLATTEN_OBJS[obj_kind](obj)

    # We ensure that there are no empty dictionaries
    remove_all_empty_dicts(data)
